# Self intro
# Adrian Hardi bin Abdul Jalil, hailing from Kuala Lumpur, 22 years of age
# Hobbies are swimming, exercising
![Pic of myself](https://gitlab.com/SugoMarbhals/introduction/-/blob/main/Me.jpg)

Strengths | Weaknesses
----------|-----------
Persistent| Brash
Loyal     | Stubborn
